# ============
#  R
# egyéni választókerületi eredmények letöltése data.frame-be

# megyék és választókerületek száma
library(dplyr)
library(ggplot2)

megye <- substr(as.character(seq(1, 20) + 100), 2, 3)
korzet <- c(18, 4, 6, 4, 7, 4, 5, 5, 6, 3, 4, 3, 2, 12, 4, 6, 3, 3, 4, 3)
oevk_df <- data_frame()

for(m1 in seq_along(megye)) {
  for(krz in seq_len(korzet[m1])) {
    oevk <- paste0("http://valtor.valasztas.hu/valtort/jsp/teljkv.jsp?EA=33&MAZ=", 
                megye[m1],"&OEVK=",
                substr(as.character(krz + 100), 2, 3), collapse = "")
    
    otab <-iconv(readLines(oevk, encoding = "ISO-8859-2"), 
                 from = "ISO-8859-2", to = "UTF-8", sub = "_")
    # jelöltek adatai összesen
    otab2 <- otab[grep("Mandátum", otab):length(otab)]
    sorsz <- grep("<tr>", otab2)
    # jelöltek egyenként
    for (s in seq_along(sorsz)) {
      kz_df <- data_frame(MEGYE = megye[m1],
                          OEVK = krz,
                          jelolt = gsub("<.*?>", "", otab2[sorsz[s]+2]),
                          part = gsub("<.*?>", "", otab2[sorsz[s]+3]),
                          szavazat = gsub("<.*?>", "", otab2[sorsz[s]+4]),
                          szav_pct = gsub("<.*?>", "", otab2[sorsz[s]+5]),
                          gyoztes  = gsub("<.*?>", "", otab2[sorsz[s]+6])
                          )
      oevk_df <- bind_rows(oevk_df, kz_df)
    }
  print(krz)}
print(megye[m1])}
